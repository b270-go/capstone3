import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
    return(
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                  
                  <Card.Body>
                    <Card.Title>No haggle, no pressure</Card.Title>
                    <Card.Text>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin faucibus eros mi, vel eleifend nibh lacinia non. Sed vulputate, sem a ultricies faucibus, odio velit euismod lacus, ac malesuada metus lectus sit amet metus. Integer euismod gravida ante, a dapibus leo convallis ac.
                    </Card.Text>
                    
                  </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                  
                  <Card.Body>
                    <Card.Title>Drive Now,Pay Later</Card.Title>
                    <Card.Text>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin faucibus eros mi, vel eleifend nibh lacinia non. Sed vulputate, sem a ultricies faucibus, odio velit euismod lacus, ac malesuada metus lectus sit amet metus. Integer euismod gravida ante, a dapibus leo convallis ac.
                    </Card.Text>
                    
                  </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                  
                  <Card.Body>
                    <Card.Title>We also buy cars</Card.Title>
                    <Card.Text>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin faucibus eros mi, vel eleifend nibh lacinia non. Sed vulputate, sem a ultricies faucibus, odio velit euismod lacus, ac malesuada metus lectus sit amet metus. Integer euismod gravida ante, a dapibus leo convallis ac.
                    </Card.Text>
                    
                  </Card.Body>
                </Card>
            </Col>
        </Row>

    )
}
