import { Fragment, useEffect, useState } from 'react';
// import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductCard';

export default function Products() {
    // Checks to see if the mock data was captured
    // console.log(coursesData);

    // State that will be used to store the courses retrieved from the database
    const [products, setProducts] = useState([]);


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products`)
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setProducts(data.map(product => {
                return(
                    <ProductCard key={product._id} productProp={product}/>
                )
            }))
        })

    }, [])

    return (
        <Fragment>
            <h1>Products</h1>
            {products}
        </Fragment>
    )
}
