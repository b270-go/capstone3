import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';


export default function Login() {

        const { user, setUser} = useContext(UserContext);

        // State hooks to store the values of the input fields
        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        // State to determine whether submit button is enabled or not
        const [isActive, setIsActive] = useState(true);

        const retrieveUserDetails = (token) => {
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data) 

                setUser({
                    id: data._id, 
                    isAdmin: data.isAdmin
                })
            })
        }

        function authenticate(e) {

            // Prevents page redirection via form submission
            e.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                }, 
                body: JSON.stringify({
                    email:email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(data.accessToken !== undefined) {
                    localStorage.setItem('token', data.accessToken);
                    retrieveUserDetails(data.accessToken)

                    Swal.fire({
                        title: "Login Succesful",
                        icon: "success",
                        text: "Enjoy shopping!"
                    })
                } else {
                    Swal.fire({
                        title: "Authentication Fail",
                        icon: "error",
                        text: "Check your log in credentials and try again!"
                    })
                }
            })

            // Set the email of the authenticated user in the local storage
            /*
                Syntax:
                localStorage.setItem('propertyName', value);
            */
            // localStorage.setItem('email', email);

            // setUser({
            //     email: localStorage.getItem('email')
            // });

            // Clear input fields after submission
            setEmail('');
            setPassword('');

            console.log(`${email} has been verified! Welcome back!`);

        }


        useEffect(() => {

            // Validation to enable submit button when all fields are populated and both passwords match
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);


    return (
        (user.id !== null)
        ?
        (
            (user.isAdmin)
            ?
            <Navigate to="/admin"/>
            :
            <Navigate to="/products"/>
        )
        :

        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }

        </Form>

    )
}
