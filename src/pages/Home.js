import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

    const data = {
        title: "It's Brake Time!",
        content: "Cars for everyone",
        destination: "/products",
        label: "Buy Now"
    }
    
    return (
        <Fragment>
            <Banner data={data}/>
            <Highlights />
        </Fragment>
    )
}