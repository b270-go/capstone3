import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView'
import AdminDashboard from './pages/AdminDashboard';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { UserProvider } from './UserContext';

function App() {

    // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({
        id: null, 
        isAdmin: null
    })

    // Function for clearing localStorage on logout
    const unsetUser = () => {
      localStorage.clear();
    }

    useEffect(() => {
        console.log(user);
        console.log(localStorage);
    }, [user])

    useEffect(() => {
        fetch("http://localhost:4000/users/details", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data) 

            if(data._id !== undefined) {

                // Sets the use state values with the user details upon successful login
                setUser({
                    id: data._id, 
                    isAdmin: data.isAdmin
                })
            } else {
                setUser({
                    id: null, 
                    isAdmin: null
                })
            }
            
        })
    }, []);

    return (
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <Container fluid>
            <AppNavbar/>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/admin" element={<AdminDashboard />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              {/* "*" is used to render paths that are not found in our routing system.*/}
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    );
}

export default App;
